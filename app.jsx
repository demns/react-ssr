import React from 'react';
import ReactDOM from 'react-dom';
import Hello from './hello';

ReactDOM.render(
  <Hello {...window.__APP_INITIAL_STATE__}/>,
  document.getElementById('react')
);
