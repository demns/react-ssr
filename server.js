require('babel-register');

const express = require('express');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();

const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

const compiler = webpack(webpackConfig);

const HelloComponent = require('./hello.jsx').default;
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const HelloFactory = React.createFactory(HelloComponent);
const template = require('./index.js');
const initialState = { name: "as</script><script>alert('123')</script>" };

app.use(require('webpack-dev-middleware')(compiler, {
  hot: true,
  stats: {
    colors: true
  }
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', function (request, response) {
  response.send(template({
    body: ReactDOMServer.renderToString(HelloFactory(initialState)),
    initialState: JSON.stringify(initialState)
  }));
});

app.listen(port);

console.log(`server started on port: ${port}`);
