var serialize = require('serialize-javascript');

module.exports = function({ body, initialState }) {
  return `
    <!DOCTYPE html>
    <html>

    <head>
      <base href="/" />
    </head>

    <body>
      <script>window.__APP_INITIAL_STATE__ = ${serialize(initialState, { isJSON: true })}</script>
      <div class='main' id="react">${body}</div>
      <script src="/bundle.js"></script>
    </body>

    </html>
  `;
};
